import { Status } from 'std/http/mod.ts';
import { createCommonResponse } from 'std/http/util.ts';

import { Config } from '../config.ts';
import { ScmClientFactory } from '../scm.ts';
import { RequestEvent } from 'libs/http_server/mod.ts';
import { createHttpError } from 'std/http/mod.ts';

declare namespace ReportRequestHandler {
    type Body = {
        repo: string;
        number: number;
        checks: CheckResult[];
    };

    type CheckResult = {
        name: string;
        result: boolean;
        details: string;
    };
}

export class ReportRequestHandler implements EventListenerObject {
    #config = new Config();

    handleEvent({ detail: { request }, resolve }: RequestEvent) {
        resolve(this.#handle(request));
    }

    async #handle(req: Request) {
        const { repo: repoName, number, checks } = await req.json() as ReportRequestHandler.Body;
        const scm = ScmClientFactory.get(this.#config.repos.get(repoName)!.community.name);
        if (!scm) {
            return createHttpError(Status.NotFound);
        }
        let result = true;
        const otd = '<td class="ui message">\n\n';
        const body = `
<table>
  <tr>
    <th>Check Name</th>
    <th>Build Result</th>
    <th>Build Details</th>
  </tr>
${
            checks.map(({ name, result: checkResult, details }) => {
                result &&= checkResult;
                return `<tr>${otd}${
                    [name, checkResult ? ':white_check_mark:**PASSED**' : ':x:**FAILED**', details].join(
                        `</td>${otd}`,
                    )
                }</td></tr>`;
            }).join()
        }
</tabel>`.trim();
        scm.pullRequest({ repo: repoName, number });
        await Promise.all([
            scm.pullRequest().createComment({ body }),
            scm.pullRequest().updateLabels({ add: [`ci/${result ? 'passed' : 'failed'}`], remove: ['ci/in-progress'] }),
        ]);
        return createCommonResponse(Status.OK);
    }
}
