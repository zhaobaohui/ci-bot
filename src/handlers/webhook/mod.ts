import { createHttpError, Status } from 'std/http/mod.ts';
import { createCommonResponse } from 'std/http/util.ts';

import { AsyncEvent } from 'libs/event/mod.ts';
import { RequestEvent } from 'libs/http_server/mod.ts';
import type { UnknownHook, Webhook } from 'libs/scm/mod.ts';
import { isUnknownHook, WebhookAction } from 'libs/scm/mod.ts';

import { Config } from '../../config.ts';
import { getLogger } from '../../log.ts';
import { ScmClientFactory } from '../../scm.ts';
import { PullRequestApproveHookHandler } from './pr_approve.ts';
import { PullRequestCommentHookHandler } from './pr_comment_create.ts';
import { PullRequestMergeHookHandler } from './pr_merge.ts';
import { PullRequestUpdateHookHandler } from './pr_update.ts';

export class WebhookRequestHandler implements EventListenerObject {
    #config = new Config();
    #log = getLogger();
    #targets = new Map<string, EventTarget>();

    constructor() {
        for (const [name, community] of this.#config.communities) {
            const target = new EventTarget();
            this.#targets.set(name, target);
            [
                WebhookAction.PR.OPEN,
                WebhookAction.PR.REOPEN,
                WebhookAction.PR.UPDATE,
                WebhookAction.PR.READY,
            ].forEach((action) => target.addEventListener(action, new PullRequestUpdateHookHandler(community)));
            target.addEventListener(WebhookAction.PR.APPROVE, new PullRequestApproveHookHandler(community));
            target.addEventListener(WebhookAction.PR.COMMENT.CREATE, new PullRequestCommentHookHandler(community));
            target.addEventListener(WebhookAction.PR.MERGE, new PullRequestMergeHookHandler(community));
        }
    }

    handleEvent({ detail: { request, args }, resolve }: RequestEvent) {
        const { community } = args!;
        resolve(this.#handle(request, community!));
    }

    async #handle(req: Request, community: string) {
        const scm = ScmClientFactory.get(community);
        if (!scm) {
            throw createHttpError(Status.NotFound);
        }
        if ('application/json' !== req.headers.get('content-type')) {
            throw createHttpError(Status.UnsupportedMediaType);
        }
        const webhook = await scm.webhook().parse(req);
        if (!webhook || isUnknownHook(webhook)) {
            this.#log.debug('unknown event');
            throw createHttpError(Status.UnprocessableEntity);
        }
        if (this.#isBot(community, webhook)) {
            throw createHttpError(Status.LoopDetected);
        }
        this.#log.debug(`event: ${webhook.action}`);
        const event = new AsyncEvent(webhook.action, { detail: webhook });
        await event.dispatchTo(this.#targets.get(community)!);
        if (!event.handled) {
            throw createHttpError(Status.BadRequest);
        }
        return createCommonResponse(Status.OK, undefined, { headers: { 'Expires': '0' } });
    }

    #isBot(community: string, webhook: Exclude<Webhook, UnknownHook>) {
        const { sender: { username } } = webhook;
        const { bot } = this.#config.communities.get(community)!;
        return username === bot;
    }
}
