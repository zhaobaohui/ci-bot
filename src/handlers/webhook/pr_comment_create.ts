import type { PullRequest, PullRequestCommentHook } from 'libs/scm/mod.ts';

import { getLogger } from '../../log.ts';
import { WebhookHandler } from './base.ts';

export class PullRequestCommentHookHandler extends WebhookHandler {
    #log = getLogger();

    protected _handle(webhook: PullRequestCommentHook) {
        const { comment, pr } = webhook;
        // TODO: command parser
        if (/^\/retest(?:$| )/.test(comment.body)) {
            return this.#retest(pr);
        }
        if (/^\/check-cla(?:$| )/.test(comment.body)) {
            return this.#checkCla(pr);
        }
        throw new Error('Method not implemented.');
    }

    async #retest(pr: PullRequest) {
        this.#log.info('command: retest');
        if (pr.state === 'open' && await this._util.checkCla(pr)) {
            await this._util.runPipeline(pr);
        }
    }

    async #checkCla(pr: PullRequest) {
        this.#log.info('command: check-cla');
        await this._util.checkCla(pr, true);
    }
}
