import type { PullRequestHook } from 'libs/scm/mod.ts';

import { WebhookHandler } from './base.ts';

export class PullRequestMergeHookHandler extends WebhookHandler {
    protected _handle(webhook: PullRequestHook) {
        const { pr } = webhook;
        return this._util.runPipeline(pr);
    }
}
