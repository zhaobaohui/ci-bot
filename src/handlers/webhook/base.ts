import type { AsyncEvent } from 'libs/event/mod.ts';
import type { Webhook } from 'libs/scm/mod.ts';

import { Config } from '../../config.ts';
import { ScmClientFactory } from '../../scm.ts';
import { WebhookHandlerUtil } from './utils.ts';

export abstract class WebhookHandler implements EventListenerObject {
    #scm;
    #util;

    protected get _scm() {
        return this.#scm;
    }

    protected get _util() {
        return this.#util;
    }

    constructor(community: Config.Community) {
        this.#scm = ScmClientFactory.get(community.name)!;
        this.#util = new WebhookHandlerUtil(community, this.#scm);
    }

    handleEvent(event: AsyncEvent<Webhook>) {
        event.resolve(this._handle(event.detail));
    }

    protected abstract _handle(webhook: Webhook): Promise<void>;
}
