use deno_core::error::AnyError;
use deno_core::ModuleSpecifier;
use deno_runtime::permissions::Permissions;
use deno_runtime::permissions::PermissionsContainer;
use deno_runtime::permissions::PermissionsOptions;
use deno_runtime::worker::MainWorker;
use deno_runtime::worker::WorkerOptions;
use deno_runtime::BootstrapOptions;
use obfstr::obfstr;
use std::env;

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), AnyError> {
    let main_module = ModuleSpecifier::from_file_path("/$out.js").unwrap();
    let mut worker = MainWorker::bootstrap_from_options(
        main_module.clone(),
        PermissionsContainer::new(
            Permissions::from_options(&include!(concat!(env!("OUT_DIR"), "/perms_opts.rs")))
                .unwrap(),
        ),
        WorkerOptions {
            bootstrap: BootstrapOptions {
                args: env::args().skip(1).collect(),
                ..Default::default()
            },
            ..Default::default()
        },
    );
    let mod_id = worker
        .js_runtime
        .load_main_module(
            &main_module,
            Some(obfstr!(include_str!("../dist/out.js")).to_string().into()),
        )
        .await?;
    worker.evaluate_module(mod_id).await?;
    worker.run_event_loop(false).await?;
    Ok(())
}
