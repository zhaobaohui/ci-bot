import * as base64 from 'std/encoding/base64.ts';
import { getLevelByName } from 'std/log/levels.ts';
import { LevelName, LogLevels } from 'std/log/mod.ts';
import * as YAML from 'std/yaml/mod.ts';

import { JsonObject } from './utils.ts';

import keypair from '../key.json' with { type: 'json' };

export declare namespace Config {
    type Init = {
        data: Config.Data;
        key: CryptoKey;
    };

    namespace Data {
        type CLA = {
            api: string;
        };
        type CommunityCLA = {
            check: boolean;
            api?: string;
            externalUrl?: string;
            id?: string;
        };
        type Jenkins = {
            api: string;
            user: string;
            token: string;
        };
        type SCM = {
            name: string;
            api: string;
        };
        type Community = {
            name: string;
            majun: string;
            scm: string;
            bot: string;
            cla: CommunityCLA;
            checkCla: boolean;
            apiToken: string;
            webhookToken: string;
        };
        type Repo = {
            name: string;
            community: string;
            job: string;
            checks: JsonObject;
            tasks: JsonObject;
        };
    }

    type Data = {
        logLevel: LevelName;
        cla: Data.CLA;
        jenkins: Data.Jenkins;
        scms: Data.SCM[];
        communities: Data.Community[];
        repos: Data.Repo[];
    };
    type SCM = Data.SCM;
    type Community = Omit<Data.Community, 'scm' | 'cla'> & {
        scm: Config.SCM;
        cla: Data.CommunityCLA & { api: string };
        repos: Map<string, Repo>;
    };
    type Repo = Omit<Data.Repo, 'community'> & { community: Community };
}

export class Config {
    static #instance: Config | null = null;

    #decoder = new TextDecoder();
    #logLevelName: LevelName = 'WARNING';
    #logLevel = LogLevels.WARNING;
    #jenkins: Config.Data.Jenkins = undefined as any;
    #scms: Map<string, Config.SCM> = undefined as any;
    #communities: Map<string, Config.Community> = undefined as any;
    #repos: Map<string, Config.Repo> = undefined as any;
    #key: CryptoKey = undefined as any;

    get logLevel() {
        return this.#logLevel;
    }

    get logLevelName() {
        return this.#logLevelName;
    }

    get jenkins() {
        return this.#jenkins;
    }

    get scms() {
        return this.#scms;
    }

    get communities() {
        return this.#communities;
    }

    get repos() {
        return this.#repos;
    }

    static async create(path: string, format?: 'json' | 'yaml') {
        if (!format) {
            const ext = path.slice(path.lastIndexOf('.') + 1);
            format = ext === 'json' ? 'json' : 'yaml';
        }
        const content = await Deno.readTextFile(path);
        const config = new Config({
            data: ('yaml' === format ? YAML : JSON).parse(content),
            key: await crypto.subtle.importKey(
                'pkcs8',
                base64.decode(keypair.privateKey),
                { name: 'RSA-OAEP', hash: 'SHA-256' },
                false,
                ['decrypt'],
            ),
        });
        await Promise.all([
            config.#decrypt(config.#jenkins.token).then((token) => {
                config.#jenkins.token = token;
            }),
            ...[...config.#communities.values()].flatMap((community) => [
                config.#decrypt(community.apiToken).then((token) => {
                    community.apiToken = token;
                }),
                config.#decrypt(community.webhookToken).then((token) => {
                    community.webhookToken = token;
                }),
            ]),
        ]);
        return config;
    }

    constructor(init?: Config.Init) {
        if (Config.#instance) {
            return Config.#instance;
        }
        if (!init) {
            throw new Error('Config not initialized');
        }
        ({ data: { logLevel: this.#logLevelName, jenkins: this.#jenkins }, key: this.#key } = init);
        this.#logLevel = getLevelByName(this.#logLevelName);
        const { data: { cla, scms, communities, repos } } = init;
        this.#scms = new Map(scms.map((scm) => [scm.name, scm]));
        this.#communities = new Map(
            communities.map((
                community,
            ) => [community.name, {
                ...community,
                scm: this.#scms.get(community.scm)!,
                cla: { ...cla, ...community.cla },
                repos: new Map(),
            }]),
        );
        this.#repos = new Map(
            repos.map((repo) => [repo.name, { ...repo, community: this.#communities.get(repo.community)! }]),
        );
        for (const community of this.#communities.values()) {
            community.repos = new Map(
                repos.filter((repo) => repo.community === community.name).map((repo) => [repo.name, {
                    ...repo,
                    community: this.#communities.get(repo.community)!,
                }]),
            );
        }
        Config.#instance = this;
    }

    async #decrypt(cipherText: string) {
        return this.#decoder.decode(await crypto.subtle.decrypt('RSA-OAEP', this.#key, base64.decode(cipherText)));
    }
}
