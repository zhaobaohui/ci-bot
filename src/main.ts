import { parse as parseArgs } from 'std/flags/mod.ts';

import { HttpRouter, HttpServer } from 'libs/http_server/mod.ts';

import { Config } from './config.ts';
import { HealthRequestHandler } from './handlers/health.ts';
import { ReportRequestHandler } from './handlers/report.ts';
import { WebhookRequestHandler } from './handlers/webhook/mod.ts';
import * as log from './log.ts';

const args = parseArgs(Deno.args, {
    alias: { f: 'format' },
    string: ['format'],
    default: {
        format: 'yaml',
    },
});
const config = await Config.create(`${args._[0]}`, args.format as any);
log.setup(config.logLevel);

new HttpServer({
    router: new HttpRouter()
        .get('/health', new HealthRequestHandler())
        .post('/webhook/:community', new WebhookRequestHandler())
        .post('/report', new ReportRequestHandler()),
});
