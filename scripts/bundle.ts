import { bundle } from 'libs/bundle/mod.ts';

const ROOT = new URL('../', import.meta.url);

await bundle(new URL('src/main.ts', ROOT), new URL('dist/out.js', ROOT), {
    sourceMap: true,
    importMap: new URL('deno.jsonc', ROOT),
    minify: true,
});
